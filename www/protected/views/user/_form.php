<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'user-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'u_first_name'); ?>
		<?php echo $form->textField($model,'u_first_name',array('size'=>60,'maxlength'=>150)); ?>
		<?php echo $form->error($model,'u_first_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'u_second_name'); ?>
		<?php echo $form->textField($model,'u_second_name',array('size'=>60,'maxlength'=>150)); ?>
		<?php echo $form->error($model,'u_second_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'u_last_name'); ?>
		<?php echo $form->textField($model,'u_last_name',array('size'=>60,'maxlength'=>150)); ?>
		<?php echo $form->error($model,'u_last_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'u_register_date'); ?>
		<?php echo $form->textField($model,'u_register_date'); ?>
		<?php echo $form->error($model,'u_register_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'u_update_date'); ?>
		<?php echo $form->textField($model,'u_update_date'); ?>
		<?php echo $form->error($model,'u_update_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'u_phone'); ?>
		<?php echo $form->textField($model,'u_phone',array('size'=>15,'maxlength'=>15)); ?>
		<?php echo $form->error($model,'u_phone'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'u_email'); ?>
		<?php echo $form->textField($model,'u_email',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'u_email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'u_skype'); ?>
		<?php echo $form->textField($model,'u_skype',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'u_skype'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'u_avatar'); ?>
		<?php echo $form->textField($model,'u_avatar',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'u_avatar'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'u_referer'); ?>
		<?php echo $form->textField($model,'u_referer',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'u_referer'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'u_discount'); ?>
		<?php echo $form->textField($model,'u_discount'); ?>
		<?php echo $form->error($model,'u_discount'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'u_type'); ?>
		<?php echo $form->textField($model,'u_type'); ?>
		<?php echo $form->error($model,'u_type'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'u_list'); ?>
		<?php echo $form->textField($model,'u_list'); ?>
		<?php echo $form->error($model,'u_list'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->