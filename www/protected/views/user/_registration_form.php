<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'user-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Поля помеченные <span class="required">*</span> обязательны к заполнению.</p>

	<?php echo $form->errorSummary($model); ?>



	<div class="row">
		<?php echo $form->labelEx($model,'u_email'); ?>
		<?php echo $form->textField($model,'u_email',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'u_email'); ?>
	</div>

    <div class="row">
        <?php echo $form->labelEx($model,'u_password'); ?>
        <?php echo $form->passwordField($model,'u_password',array('size'=>60,'maxlength'=>100)); ?>
        <?php echo $form->error($model,'u_password'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'u_phone'); ?>
        <?php echo $form->textField($model,'u_phone',array('size'=>15,'maxlength'=>15)); ?>
        <?php echo $form->error($model,'u_phone'); ?>
    </div>

	<div class="row">
		<?php echo $form->labelEx($model,'u_referer'); ?>
		<?php echo $form->textField($model,'u_referer',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'u_referer'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Зарегистрироваться' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->