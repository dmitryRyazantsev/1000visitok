<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'u_id'); ?>
		<?php echo $form->textField($model,'u_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'u_first_name'); ?>
		<?php echo $form->textField($model,'u_first_name',array('size'=>60,'maxlength'=>150)); ?>

	</div>

	<div class="row">
		<?php echo $form->label($model,'u_second_name'); ?>
		<?php echo $form->textField($model,'u_second_name',array('size'=>60,'maxlength'=>150)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'u_last_name'); ?>
		<?php echo $form->textField($model,'u_last_name',array('size'=>60,'maxlength'=>150)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'u_register_date'); ?>
		<?php echo $form->textField($model,'u_register_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'u_update_date'); ?>
		<?php echo $form->textField($model,'u_update_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'u_phone'); ?>
		<?php echo $form->textField($model,'u_phone',array('size'=>15,'maxlength'=>15)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'u_email'); ?>
		<?php echo $form->textField($model,'u_email',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'u_skype'); ?>
		<?php echo $form->textField($model,'u_skype',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'u_avatar'); ?>
		<?php echo $form->textField($model,'u_avatar',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'u_referer'); ?>
		<?php echo $form->textField($model,'u_referer',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'u_discount'); ?>
		<?php echo $form->textField($model,'u_discount'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'u_type'); ?>
		<?php echo $form->textField($model,'u_type'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'u_list'); ?>
		<?php echo $form->textField($model,'u_list'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->