<?php
/* @var $this UserController */
/* @var $data User */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('u_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->u_id), array('view', 'id'=>$data->u_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('u_first_name')); ?>:</b>
	<?php echo CHtml::encode($data->u_first_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('u_second_name')); ?>:</b>
	<?php echo CHtml::encode($data->u_second_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('u_last_name')); ?>:</b>
	<?php echo CHtml::encode($data->u_last_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('u_register_date')); ?>:</b>
	<?php echo CHtml::encode($data->u_register_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('u_update_date')); ?>:</b>
	<?php echo CHtml::encode($data->u_update_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('u_phone')); ?>:</b>
	<?php echo CHtml::encode($data->u_phone); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('u_email')); ?>:</b>
	<?php echo CHtml::encode($data->u_email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('u_skype')); ?>:</b>
	<?php echo CHtml::encode($data->u_skype); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('u_avatar')); ?>:</b>
	<?php echo CHtml::encode($data->u_avatar); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('u_referer')); ?>:</b>
	<?php echo CHtml::encode($data->u_referer); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('u_discount')); ?>:</b>
	<?php echo CHtml::encode($data->u_discount); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('u_type')); ?>:</b>
	<?php echo CHtml::encode($data->u_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('u_list')); ?>:</b>
	<?php echo CHtml::encode($data->u_list); ?>
	<br />

	*/ ?>

</div>