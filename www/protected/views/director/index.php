<?php
/**
 * Created by PhpStorm.
 * User: Дмитрий
 * Date: 14.01.14
 * Time: 0:15
 * @var $this DirectorController
 * @var $model User
 */
?>
<?php
    Yii::app()->clientScript->registerScript('search', "
    $('.search-button').click(function(){
        $('.search-form').toggle();
        return false;
    });
    $('.search-form form').submit(function(){
        $('#managers-grid').yiiGridView('update', {
            data: $(this).serialize()
        });
        return false;
    });
    ");
?>
<section class="table" style="margin-top: 0;">
    <div class="select-search-wrapper">
        <div class="yellow button add-manager"><a href="<?=$this->createUrl('director/addmanager')?>">Добавить менеджера</a></div>
        <div class="action-select">
            <span>С выделенными:</span>
            <select placeholder="Выберите макет">
                <option value="" default selected>Выберите действие</option>
                <option value="unblock">Разблокировать</option>
                <option value="block">Заблокировать</option>
            </select>
            <div class="yellow button make"><a href="#addmanager">Ок</a></div>
        </div>

        <div class="search-form">
         <?php
         $form=$this->beginWidget('CActiveForm', array(
            'action'=>Yii::app()->createUrl($this->route),
            'method'=>'get',
        ));
        ?>

        <div class="search">
            <!-- <input type="text" name="search" id="search" value=""> -->
            <?php echo $form->textField($model,'u_last_name', array('id' => "search", )); ?>
            <?php //echo CHtml::submitButton('', array('id' => 'search-submit')); ?>
            <button type="submit" name="submit" id="search-submit">
        </div>
        <?php $this->endWidget(); ?>
        </div>
    </div>


    <?php
    $this->widget('zii.widgets.grid.CGridView', array(
        'id' => 'managers-grid',
        'dataProvider' => $model->searchManagers(),
        'filter' => $model,
        'columns' => array(
            array(
                'name' => 'u_register_date',
                'header' => 'Дата регистрации',
                'type' => 'date',
                'value' => '$data->u_register_date',
                'filter' => true,
                //'search' => true,
            ),
            array(
                'name' => 'u_last_name',
                'header' => 'ФИО',
                'value' => '$data->getFio($data->u_id)',
                //'filter' => true,
            ),
            'u_email',
            'u_phone',

            array(
            'class' => 'CButtonColumn',
            ),
        ),
            'template'=>"{items}\n{pager}",
    ));

    ?>
    <span class="no-results">Ничего не найдено</span>
</section>