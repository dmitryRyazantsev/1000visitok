<?php
/**
 * Created by PhpStorm.
 * User: Дмитрий
 * Date: 14.01.14
 * Time: 10:14
 * @var $this DirectorController
 * @var $model User
 */
?>
<section class="account">
    <img src="<?= Yii::app()->getBaseUrl(true) ?>/img/userpic.png" class="userpic"/>
    <div class="info">
        <div class="main-info">
            <div style="overflow: hidden;">
                <span class="name"><?= $model->getFio($model->u_id)?></span>
                <div class="main-info-actions">
                    <a href="#mail" rel="leanModal"><img src="<?= Yii::app()->getBaseUrl(true) ?>/img/mail.png" /></a>
                    <a href="#delete-account" rel="leanModal"><img src="<?= Yii::app()->getBaseUrl(true) ?>/img/delete_account.png" /></a>
                </div>
            </div>
            <span class="date">Дата регистрации: <?= date("d-m-Y",$model->u_register_date) ?></span>
        </div>
        <div class="contact-money-info">
            <div>
                <span class="info-type">Email:</span><span class="info-content"><?= $model->u_email ?></span>
                </br>
                <span class="info-type">Телефон:</span><span class="info-content">+7 <?= $model->u_phone ?></span>
                </br>
                <span class="info-type">Skype:</span><span class="info-content"><?= $model->u_skype ?></span>
            </div>

        </div>
    </div>
</section>