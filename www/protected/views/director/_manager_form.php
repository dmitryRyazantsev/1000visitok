<?php
/**
 * Created by PhpStorm.
 * User: Дмитрий
 * Date: 14.01.14
 * Time: 10:25
 */ ?>

<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'addmanager',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation'=>false,
    )); ?>

    <p class="note">Поля помеченные <span class="required">*</span> обязательны к заполнению.</p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model,'u_first_name'); ?>
        <?php echo $form->textField($model,'u_first_name',array('size'=>60,'maxlength'=>150)); ?>
        <?php echo $form->error($model,'u_first_name'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'u_second_name'); ?>
        <?php echo $form->textField($model,'u_second_name',array('size'=>60,'maxlength'=>150)); ?>
        <?php echo $form->error($model,'u_second_name'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'u_last_name'); ?>
        <?php echo $form->textField($model,'u_last_name',array('size'=>60,'maxlength'=>150)); ?>
        <?php echo $form->error($model,'u_last_name'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'u_email'); ?>
        <?php echo $form->textField($model,'u_email',array('size'=>60,'maxlength'=>100)); ?>
        <?php echo $form->error($model,'u_email'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'u_password'); ?>
        <?php echo $form->passwordField($model,'u_password',array('size'=>60,'maxlength'=>100)); ?>
        <?php echo $form->error($model,'u_password'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'u_phone'); ?>
        <?php echo $form->textField($model,'u_phone',array('size'=>15,'maxlength'=>15)); ?>
        <?php echo $form->error($model,'u_phone'); ?>
    </div>



    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Добавить' : 'Save'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->