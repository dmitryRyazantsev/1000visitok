<?php
/**
 * Created by PhpStorm.
 * User: Дмитрий
 * Date: 14.01.14
 * Time: 0:01
 */

class DirectorController extends Controller
{
    public function actionView($id)
    {
        $this->render('view',array(
            'model'=>$this->loadModel($id),
        ));
    }

    public function actionManagers(){
        $model = new User();
        $model->unsetAttributes();

        if(isset($_GET['User']))
            $model->attributes=$_GET['User'];

        $this->render('index', array(
            'model' => $model,
        ));
    }

    public function actionAddmanager()
    {
        $model = new User();
        if(isset($_POST['User']))
        {
            $model->attributes = $_POST['User'];
            if ($model->validate() && $model->checkEmail($model->u_email) === true)
            {
                $model->u_type = 2;
                $model->save(false);
                $this->redirect($this->createUrl('director/managers'));
            }

        }
        $this->render('add_manager', array('model' => $model));
    }

    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['User']))
        {
            $model->attributes=$_POST['User'];
            if($model->save())
                $this->redirect(array('view','id'=>$model->u_id));
        }

        $this->render('update',array(
            'model'=>$model,
        ));
    }

    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if(!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('director'));
    }
    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return User the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model=User::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }
}
?>