<?php

/**
 * This is the model class for table "user_status_list".
 *
 * The followings are the available columns in table 'user_status_list':
 * @property integer $ul_id
 * @property string $ul_title
 * @property string $ul_title_ru
 *
 * The followings are the available model relations:
 * @property User[] $users
 */
class UserStatusList extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user_status_list';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ul_title', 'required'),
			array('ul_title, ul_title_ru', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ul_id, ul_title, ul_title_ru', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'users' => array(self::HAS_MANY, 'User', 'u_list'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ul_id' => 'Ul',
			'ul_title' => 'Ul Title',
			'ul_title_ru' => 'Ul Title Ru',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ul_id',$this->ul_id);
		$criteria->compare('ul_title',$this->ul_title,true);
		$criteria->compare('ul_title_ru',$this->ul_title_ru,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UserStatusList the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    /**
     * Статический метод для выбора всех статусов пользователей.
     * @return array
     */
    public static function userStatuses()
    {
        $model = UserStatusList::model()->findAll();
        $list = array();
        foreach ($model as $item)
        {
            $list[] = $item->ul_title_ru;
        }
        return $list;
    }
}
