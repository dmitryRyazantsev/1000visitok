<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $u_id
 * @property string $u_first_name
 * @property string $u_second_name
 * @property string $u_last_name
 * @property integer $u_register_date
 * @property integer $u_update_date
 * @property string $u_phone
 * @property string $u_email
 * @property string $u_password
 * @property string $u_skype
 * @property string $u_avatar
 * @property string $u_referer
 * @property integer $u_discount
 * @property integer $u_type
 * @property integer $u_list
 * @property string $_fio
 *
 * The followings are the available model relations:
 * @property Order[] $orders
 * @property Product[] $products
 * @property UserType $uType
 * @property UserStatusList $uList
 * @property UserWallet[] $userWallets
 */
class User extends CActiveRecord
{
    private  $_fio;
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'user';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('u_email', 'required'),
            array('u_register_date, u_update_date, u_discount, u_type, u_list', 'numerical', 'integerOnly'=>true),
            array('u_first_name, u_second_name, u_last_name', 'length', 'max'=>150),
            array('u_phone', 'length', 'max'=>15),
           // array('u_password', 'length', 'min' => 8),
           // array('u_password', 'length', 'max' => 16),
            array('u_email, u_skype', 'length', 'max'=>100),
            array('u_email', 'email'),
            array('u_password, u_avatar, u_referer', 'length', 'max'=>255),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('u_id, u_first_name, u_second_name, u_last_name, u_register_date, u_update_date, u_phone, u_email, u_password, u_skype, u_avatar, u_referer, u_discount, u_type, u_list', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'orders' => array(self::HAS_MANY, 'Order', 'o_user_id'),
            'products' => array(self::HAS_MANY, 'Product', 'p_author_id'),
            'uType' => array(self::BELONGS_TO, 'UserType', 'u_type'),
            'uList' => array(self::BELONGS_TO, 'UserStatusList', 'u_list'),
            'userWallets' => array(self::HAS_MANY, 'UserWallet', 'uw_user_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'u_id' => 'U',
            'u_first_name' => 'Имя',
            'u_second_name' => 'Отчество',
            'u_last_name' => 'Фамилия',
            'u_register_date' => 'Дата регистрации',
            'u_update_date' => 'Последнее обновление',
            'u_phone' => 'Телефон',
            'u_email' => 'E-mail',
            'u_password' => 'Пароль',
            'u_skype' => 'Skype',
            'u_avatar' => 'Avatar',
            'u_referer' => 'Referer',
            'u_discount' => 'Discount',
            'u_type' => 'Type',
            'u_list' => 'List',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('u_id',$this->u_id);
        $criteria->compare('u_first_name',$this->u_first_name,true);
        $criteria->compare('u_second_name',$this->u_second_name,true);
        $criteria->compare('u_last_name',$this->u_last_name,true);
        $criteria->compare('u_register_date',$this->u_register_date);
        $criteria->compare('u_update_date',$this->u_update_date);
        $criteria->compare('u_phone',$this->u_phone,true);
        $criteria->compare('u_email',$this->u_email,true);
        $criteria->compare('u_password',$this->u_password,true);
        $criteria->compare('u_skype',$this->u_skype,true);
        $criteria->compare('u_avatar',$this->u_avatar,true);
        $criteria->compare('u_referer',$this->u_referer,true);
        $criteria->compare('u_discount',$this->u_discount);
        $criteria->compare('u_type',$this->u_type);
        $criteria->compare('u_list',$this->u_list);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return User the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    /**
    * beforeSave переопределя для устарновки автоматической даты и
    * генерации хеша пароля.
    */
    protected function beforeSave()
    {
        if($this->getIsNewRecord())
        {
            $this->u_register_date = $this->u_update_date = time();
            $this->u_password = $this->hashPassword($this->u_password);
        }
        else
        {
            $this->u_update_date = time();
        }
        return parent::beforeSave();
    }

    /**
    * Function for create and hash password
    */
    public function hashPassword($password)
    {
        return CPasswordHelper::hashPassword($password);
    }

    /**
    * Function for verification hash and password
    */
    public function verifyPassword($password)
    {
        return CPasswordHelper::verifyPassword($password, $this->u_password);
    }

    /**
    * Check user email or phone
     */
    public function checkEmail($email)
    {
            $criteria = new CDbCriteria();
            $criteria->condition = "u_email = :email";
            $criteria->params = array(":email" => $email);
            if(User::model()->find($criteria))
            {
                return false;
            }
            else
                return true;
    }

    public function getFio($id)
    {
        $id = $this->u_id;
        $fio = $this->findByPk($id);
        return $this->_fio = $fio->u_last_name." ".$fio->u_first_name." ".$fio->u_second_name;
    }

    /**
     * @return CActiveDataProvider
     * Функция фильтрации и поиска менеджеров.
     */
    public function searchManagers()
    {
        //$this->u_fio;
        $criteria=new CDbCriteria;
        $criteria->condition = "u_type = 2";

        /*$sort = new CSort();
        $sort->attributes = array(
            'u_fio' => array(
            'asc' => 'u_last_name, u_first_name, u_second_name',
            'desc' => 'u_last_name DESC, u_first_name DESC, u_second_name DESC',
            ),
            '*',
        );*/

        $criteria->compare('u_id',$this->u_id);
        $criteria->compare('u_last_name',$this->u_last_name,true);
        $criteria->compare('u_register_date',$this->u_register_date);
        $criteria->compare('u_phone',$this->u_phone,true);
        $criteria->compare('u_email',$this->u_email,true);
        $criteria->compare('u_password',$this->u_password,true);
        $criteria->compare('u_skype',$this->u_skype,true);
        $criteria->compare('u_avatar',$this->u_avatar,true);
        $criteria->compare('u_type',$this->u_type);
        $criteria->compare('u_list',$this->u_list);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            //'sort' => $sort,
        ));
    }

}