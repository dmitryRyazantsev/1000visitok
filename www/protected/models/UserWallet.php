<?php

/**
 * This is the model class for table "user_wallet".
 *
 * The followings are the available columns in table 'user_wallet':
 * @property integer $uw_id
 * @property string $uw_title
 * @property string $uw_wallet
 * @property integer $uw_create_date
 * @property integer $uw_update_date
 * @property string $uw_label
 * @property integer $uw_user_id
 *
 * The followings are the available model relations:
 * @property User $uwUser
 */
class UserWallet extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user_wallet';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('uw_title, uw_wallet, uw_create_date, uw_user_id', 'required'),
			array('uw_create_date, uw_update_date, uw_user_id', 'numerical', 'integerOnly'=>true),
			array('uw_title', 'length', 'max'=>200),
			array('uw_wallet, uw_label', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('uw_id, uw_title, uw_wallet, uw_create_date, uw_update_date, uw_label, uw_user_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'uwUser' => array(self::BELONGS_TO, 'User', 'uw_user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'uw_id' => 'Uw',
			'uw_title' => 'Uw Title',
			'uw_wallet' => 'Uw Wallet',
			'uw_create_date' => 'Uw Create Date',
			'uw_update_date' => 'Uw Update Date',
			'uw_label' => 'Uw Label',
			'uw_user_id' => 'Uw User',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('uw_id',$this->uw_id);
		$criteria->compare('uw_title',$this->uw_title,true);
		$criteria->compare('uw_wallet',$this->uw_wallet,true);
		$criteria->compare('uw_create_date',$this->uw_create_date);
		$criteria->compare('uw_update_date',$this->uw_update_date);
		$criteria->compare('uw_label',$this->uw_label,true);
		$criteria->compare('uw_user_id',$this->uw_user_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UserWallet the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
