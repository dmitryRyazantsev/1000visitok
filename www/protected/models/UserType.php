<?php

/**
 * This is the model class for table "user_type".
 *
 * The followings are the available columns in table 'user_type':
 * @property integer $ut_id
 * @property string $ut_title
 * @property string $ut_title_ru
 *
 * The followings are the available model relations:
 * @property User[] $users
 */
class UserType extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user_type';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ut_title, ut_title_ru', 'required'),
			array('ut_title, ut_title_ru', 'length', 'max'=>45),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ut_id, ut_title, ut_title_ru', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'users' => array(self::HAS_MANY, 'User', 'u_type'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ut_id' => 'Ut',
			'ut_title' => 'Ut Title',
			'ut_title_ru' => 'Ut Title Ru',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ut_id',$this->ut_id);
		$criteria->compare('ut_title',$this->ut_title,true);
		$criteria->compare('ut_title_ru',$this->ut_title_ru,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UserType the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
    /**
     * Статический метод для выбора все типов пользователей.
     * @return array
    */
    public static function userTypes()
    {
        $model = UserType::model()->findAll();
        $list = array();
        $i = 0;
        foreach ($model as $item)
        {
            $i++;
            $list[$i] = $item->ut_title_ru;
        }
        return $list;
    }
}
