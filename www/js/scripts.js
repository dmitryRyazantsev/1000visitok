function chooseFile() {$("#files").click();}
function check(n) {
    var row = n.parentNode.parentNode;
    if($(row).hasClass("checked")){
	    $(row).removeClass("checked");	
    } else {
    	$(row).addClass("checked");	
    }
    /*
var cols = row.getElementsByTagName("td");
    var i = 0;
*/
    /*
var cols = row.getElementsByTagName("td");
    var i = 0;

    while (i < cols.length) {
        var val = cols[i].childNodes[0].nodeValue
        if (val != null) {
            alert(val);
        }
        i++;
    }
*/
}
$(document).ready(function(){


	$("#tabs>a").click( function () {
        
        $("#tabs>a").removeClass("current");
        $(this).addClass("current");
        
        $(".tabs_content>div").hide();
        t_content=$(this).attr("href");
        $(t_content).show();

        return false
    })
	$("#tabs>a:first").trigger("click");

	$(".cards .use-own, .stickers .use-own").click(function(){
		if($(".cards, .stickers").has(".modal-form").length == 0){
			$(this).append("<div class='use-own-form modal-form'><div class='modal_close'></div><form id='' class='' autocomplete='' method='post' novalidate=''><div id='filelist'></div><span class='choose_file' onclick='chooseFile();'>Выберите макет</span><input type='file' multiple='multiple' name='uploadedfile[]' id='files'/><select placeholder='Выберите макет'><option value='' default selected>Выберите действие</option><option value='unblock'>Разблокировать</option><option value='block'>Заблокировать</option></select><label>Теги:</label><input name='tags' id='tags' placeholder='автомобили, авто, карс, car' type='text' maxlength='255' value=''><button id='submit' type='submit'>Продолжить</button></form></div>");
			$(this).append("<div id='block-close-overlay' style='display: block; opacity: 1;'></div>")
		}
	});
	$(".cards .block").click(function(){
		if($(".cards").has(".modal-form").length == 0){
			$(this).append("<div class='use-form modal-form'><div class='modal_close'></div><div class='radio'><input type='radio' name='group1' value='free'><span class='radio-bold'>Шаблон бесплатно</span><span class='radio-normal'>макет останется на сайте в открытом доступе</span></div><div value='2' class='radio'><input type='radio' name='group1' value='buy'><span class='radio-bold'>Выкупить макет (500 руб.)</span><span class='radio-normal'>макет навсегда исчезнет с сайта</span></div><button id='submit' type='submit'>Выбрать и продолжить</button></form></div>");
			 $(this).append("<div id='block-close-overlay' style='display: block; opacity: 1;'></div>")
		}
	});
	$(document).on("click", ".modal-form .modal_close, #block-close-overlay", function(){
		$(".cards .modal-form, .stickers .modal-form").remove();
		$("#block-close-overlay").remove()
	});
	/*
		$(".cards .block").click(function(){
			$(this).append("<div class=''></div>)
		})
	*/
	if($( document ).width() < 600){
		var liCount = $("ul.tabs li.passive, ul.tabs li.active").size();
		var menu = liCount * 125;
		menu = menu + 'px';
		$("ul.tabs").css('width',menu);	
	}
	
	$( "ul.menu li" ).has( "ul" ).mouseover(function(){
		$(this).addClass('hovered');
	}).mouseout(function(){
    	$(this).removeClass('hovered');
	});
	
	$( "ul.menu li" ).has( "ul" ).click(function(){
		if($(this).children('ul').css('display') == 'none') {
			$(this).children('ul').css('display', 'block');	
		} else {
			$(this).children('ul').css('display', 'none');	
		}
	});
	
	$.tablesorter.addParser({
        // set a unique id 
        id: 'rangesort',
        is: function (s) {
            // return false so this parser is not auto detected 
            return false;
        },
        format: function (s, table, cell, cellIndex) {
            // get data attributes from $(cell).attr('data-something');
            // check specific column using cellIndex
            return $(cell).attr('sort');
        },
        // set type, either numeric or text 
        type: 'text'
    });
	$("table.layouts").tablesorter({
		dateFormat: 'dd.mm.yyyy',
		headers: {1: { sorter: 'eudate' },2: { sorter: 'text' },3: { sorter: 'text' },4: { sorter: 'digit' }}
	});
	$(".money-history table").tablesorter({
		dateFormat: 'dd.mm.yyyy',
		headers: {0: { sorter: 'eudate' },1: { sorter: 'text' },2: { sorter: 'text' },3: { sorter: 'digit' }}
	});
	$("table.webmaster").tablesorter({
		dateFormat: 'dd.mm.yyyy',
		headers: {0: { sorter: 'eudate' },1: { sorter: 'text' },2: { sorter: 'text' },3: { sorter: 'digit' },4: { sorter: 'digit' }}
	});
	$("table.orders").tablesorter({
		dateFormat: 'dd.mm.yyyy',
		headers: {0: { sorter: 'rangesort' },1: { sorter: 'digit' },2: { sorter: 'eudate' },3: { sorter: 'digit' },4: { sorter: false },5: { sorter: 'text' },6: { sorter: 'digit' },7: { sorter: false },8: { sorter: false }}
	});
	$("table.customers").tablesorter({
		dateFormat: 'dd.mm.yyyy',
		headers: {0: { sorter: false },1: { sorter: 'digit' },2: { sorter: 'eudate' },3: { sorter: 'text' },4: { sorter: 'text' },5: { sorter: false },6: { sorter: false }}
	});
	$(function(){$('a[rel*=leanModal]').leanModal({ closeButton: ".modal_close" });});
	
	$('table a[rel*=leanModal]').click(function(){
		var email = $(this).parent().parent().children(".email").attr('value');
		$('input[id*=email]').val(email);
	});
	
	
	$('#files').change(function(){
		var file = this.files;
		var filenames = '';
		for (var i = 0; i < this.files.length; i++){
			/* filename += file.name + '</br>'; */
			filenames += '<div class="item"><div class="remove-file"></div>' + file[i]['name'] + '</div>';
		}
		$("#filelist").html(filenames);
		console.log(file);
		console.log(filenames);
	});
	
	$(document).on('click', '.remove-file', function(){
		$(this).parent().remove();
	});
	
	// Write on keyup event of keyword input element
	$("#search").keyup(function(){
		// When value of the input is not blank
		$(".select select").val("");
		if( $(this).val() != "")
		{
			// Show only matching TR, hide rest of them
			$(".layouts tbody>tr, .webmaster tbody>tr, .customers tbody>tr").hide();
			
			$(".layouts td:contains-ci('" + $(this).val() + "'), .customers td:contains-ci('" + $(this).val() + "'), .webmaster td:contains-ci('" + $(this).val() + "')").parent("tr").show();	
			var count = $('.layouts tr:not([style*="display: none"]), .customers tr:not([style*="display: none"]), .webmaster tr:not([style*="display: none"])').length;
			if(count == 1){
				$('.no-results').css('display','block');
			} else {
				$('.no-results').css('display','none');
			}
		}
		else
		{
			// When there is no input or clean again, show everything back
			$(".layouts tbody>tr, .webmaster tbody>tr, .customers tbody>tr").show();
			var count = $('.layouts tr:not([style*="display: none"]), .customers tr:not([style*="display: none"]), .webmaster tr:not([style*="display: none"])').length;
			if(count == 1){
				$('.no-results').css('display','block');
			} else {
				$('.no-results').css('display','none');
			}
		}
	});
	
	$(document).ready(function(){
		$(".select select").change(function (){
			var val = $(this).val();
			if (val == ""){
				$(".layouts tbody tr").show();
				/* return true; */
			} else {
				$(".layouts tbody tr").show();
				$(".layouts tbody tr td.type:not(:contains('" + val + "'))").each(function(){
					$(this).parent().hide();
					$("#search").val("");
				});
				/* $("#Smth").hide(); */
			}
		});
	});
	
});

$.extend($.expr[":"], 
{
    "contains-ci": function(elem, i, match, array) 
	{
		return (elem.textContent || elem.innerText || $(elem).text() || "").toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
	}
});