-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Янв 13 2014 г., 04:26
-- Версия сервера: 5.5.25
-- Версия PHP: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `visitki`
--

-- --------------------------------------------------------

--
-- Структура таблицы `order`
--

CREATE TABLE IF NOT EXISTS `order` (
  `o_id` int(11) NOT NULL AUTO_INCREMENT,
  `o_user_id` int(11) NOT NULL,
  `o_shipping` int(11) NOT NULL,
  `o_status` int(11) NOT NULL,
  `o_timeline` int(11) NOT NULL COMMENT 'продолжительность',
  `o_payment_type` int(11) NOT NULL,
  `o_quantity` int(11) NOT NULL DEFAULT '1' COMMENT 'Колличество',
  `o_total_price` int(11) NOT NULL,
  `o_create_date` int(11) NOT NULL,
  `o_update_date` int(11) DEFAULT NULL,
  `o_fio` varchar(255) DEFAULT NULL,
  `o_email` varchar(100) DEFAULT NULL,
  `o_phone` varchar(15) DEFAULT NULL,
  `o_shipping_adress` varchar(255) DEFAULT NULL,
  `o_discount` int(11) DEFAULT NULL,
  `o_document` varchar(255) DEFAULT NULL COMMENT 'Документ со счетом для безнала',
  PRIMARY KEY (`o_id`),
  KEY `fk_order_user1_idx` (`o_user_id`),
  KEY `fk_order_shipping1_idx` (`o_shipping`),
  KEY `fk_order_order_timeline1_idx` (`o_timeline`),
  KEY `fk_order_order_status1_idx` (`o_status`),
  KEY `fk_order_order_payment_type1_idx` (`o_payment_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `order_products`
--

CREATE TABLE IF NOT EXISTS `order_products` (
  `op_order_id` int(11) NOT NULL,
  `op_product_id` int(11) NOT NULL,
  `op_update_time` int(11) NOT NULL,
  PRIMARY KEY (`op_order_id`,`op_product_id`),
  KEY `op_product_id` (`op_product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `order_status`
--

CREATE TABLE IF NOT EXISTS `order_status` (
  `os_id` int(11) NOT NULL AUTO_INCREMENT,
  `os_name` varchar(255) NOT NULL,
  PRIMARY KEY (`os_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `order_status`
--

INSERT INTO `order_status` (`os_id`, `os_name`) VALUES
(1, 'в обработке'),
(2, 'передан курьеру'),
(3, 'ожидает подтверждения');

-- --------------------------------------------------------

--
-- Структура таблицы `order_timeline`
--

CREATE TABLE IF NOT EXISTS `order_timeline` (
  `otl_id` int(11) NOT NULL AUTO_INCREMENT,
  `otl_name` varchar(255) NOT NULL,
  `otl_css_class` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`otl_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `order_timeline`
--

INSERT INTO `order_timeline` (`otl_id`, `otl_name`, `otl_css_class`) VALUES
(1, '4 часа', NULL),
(2, '2 дня', NULL),
(3, '4 дня', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `payment_type`
--

CREATE TABLE IF NOT EXISTS `payment_type` (
  `pt_id` int(11) NOT NULL AUTO_INCREMENT,
  `pt_name` varchar(255) NOT NULL,
  `pt_description` text,
  `pt_css_class` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`pt_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `payment_type`
--

INSERT INTO `payment_type` (`pt_id`, `pt_name`, `pt_description`, `pt_css_class`) VALUES
(1, 'Курьеру', NULL, NULL),
(2, 'Webmoney', NULL, NULL),
(3, 'Б/н расчет', NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `p_id` int(11) NOT NULL AUTO_INCREMENT,
  `p_name` varchar(255) NOT NULL,
  `p_price` int(11) NOT NULL,
  `p_description` text NOT NULL,
  `p_preview_img` varchar(255) NOT NULL,
  `p_published` int(11) NOT NULL DEFAULT '1' COMMENT 'Опубликован или нет.',
  `p_side` varchar(2) DEFAULT NULL COMMENT 'Колличество сторон/страниц',
  `p_type` int(11) NOT NULL,
  `p_size` int(11) NOT NULL COMMENT 'размеры продукта',
  `p_author_id` int(11) NOT NULL,
  `p_category` int(11) NOT NULL,
  `p_template` int(11) NOT NULL,
  PRIMARY KEY (`p_id`),
  KEY `fk_product_product_size1_idx` (`p_size`),
  KEY `fk_product_product_type1_idx` (`p_type`),
  KEY `fk_product_user1_idx` (`p_author_id`),
  KEY `fk_product_product_category1_idx` (`p_category`),
  KEY `fk_product_product_template1_idx` (`p_template`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Товары' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `product_category`
--

CREATE TABLE IF NOT EXISTS `product_category` (
  `pc_id` int(11) NOT NULL AUTO_INCREMENT,
  `pc_name` varchar(255) NOT NULL,
  `pc_name_ru` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`pc_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Таблица тегов для товара' AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `product_category`
--

INSERT INTO `product_category` (`pc_id`, `pc_name`, `pc_name_ru`) VALUES
(1, 'Природа', NULL),
(2, 'Перевозки', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `product_size`
--

CREATE TABLE IF NOT EXISTS `product_size` (
  `ps_id` int(11) NOT NULL AUTO_INCREMENT,
  `ps_size` varchar(255) NOT NULL,
  `ps_unit` varchar(45) NOT NULL COMMENT 'еденица измерения',
  PRIMARY KEY (`ps_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='размеры для товаров' AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `product_size`
--

INSERT INTO `product_size` (`ps_id`, `ps_size`, `ps_unit`) VALUES
(1, '25x60', 'мм'),
(2, '30x10', 'см');

-- --------------------------------------------------------

--
-- Структура таблицы `product_template`
--

CREATE TABLE IF NOT EXISTS `product_template` (
  `ptmpl_int` int(11) NOT NULL AUTO_INCREMENT,
  `ptmpl_img` varchar(255) NOT NULL,
  `ptmp_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ptmpl_int`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `product_type`
--

CREATE TABLE IF NOT EXISTS `product_type` (
  `pt_id` int(11) NOT NULL AUTO_INCREMENT,
  `pt_name` varchar(255) NOT NULL,
  `pt_name_ru` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`pt_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `product_type`
--

INSERT INTO `product_type` (`pt_id`, `pt_name`, `pt_name_ru`) VALUES
(1, 'Визитки', NULL),
(2, 'Календари', NULL),
(3, 'Флаеры', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `shipping`
--

CREATE TABLE IF NOT EXISTS `shipping` (
  `s_id` int(11) NOT NULL AUTO_INCREMENT,
  `s_name` varchar(255) NOT NULL,
  `s_price` int(11) NOT NULL,
  `s_description` text,
  PRIMARY KEY (`s_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `shipping`
--

INSERT INTO `shipping` (`s_id`, `s_name`, `s_price`, `s_description`) VALUES
(1, 'Самовывоз', 0, NULL),
(2, 'Доставка курьером', 300, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `u_id` int(11) NOT NULL AUTO_INCREMENT,
  `u_first_name` varchar(150) NOT NULL,
  `u_second_name` varchar(150) NOT NULL,
  `u_last_name` varchar(150) NOT NULL,
  `u_register_date` int(11) NOT NULL,
  `u_update_date` int(11) DEFAULT NULL,
  `u_phone` varchar(15) NOT NULL,
  `u_email` varchar(100) NOT NULL,
  `u_skype` varchar(100) DEFAULT NULL,
  `u_avatar` varchar(255) DEFAULT NULL,
  `u_referer` varchar(255) DEFAULT NULL,
  `u_discount` int(11) NOT NULL DEFAULT '0' COMMENT 'Скидка',
  `u_type` int(11) NOT NULL COMMENT 'Тип юзера (менеджер, дизайнер, админ и ...)',
  `u_list` int(11) NOT NULL DEFAULT '1' COMMENT 'Списко пользователей',
  PRIMARY KEY (`u_id`),
  KEY `fk_user_user_type_idx` (`u_type`),
  KEY `fk_user_user_lists1_idx` (`u_list`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Пользователи' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `user_status_list`
--

CREATE TABLE IF NOT EXISTS `user_status_list` (
  `ul_id` int(11) NOT NULL AUTO_INCREMENT,
  `ul_title` varchar(255) NOT NULL,
  `ul_title_ru` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ul_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Пользовательские списки.\n(черный список, постоянный клиент и /* comment truncated */ /* тп)*/' AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `user_status_list`
--

INSERT INTO `user_status_list` (`ul_id`, `ul_title`, `ul_title_ru`) VALUES
(1, 'usual', 'Обычные'),
(2, 'blacklist', 'Blacklist'),
(3, 'locked', 'Заблокирован');

-- --------------------------------------------------------

--
-- Структура таблицы `user_type`
--

CREATE TABLE IF NOT EXISTS `user_type` (
  `ut_id` int(11) NOT NULL AUTO_INCREMENT,
  `ut_title` varchar(45) NOT NULL,
  `ut_title_ru` varchar(45) NOT NULL,
  PRIMARY KEY (`ut_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `user_type`
--

INSERT INTO `user_type` (`ut_id`, `ut_title`, `ut_title_ru`) VALUES
(1, 'client', 'Клиент'),
(2, 'manager', 'Менеджер'),
(3, 'webmaster', 'Вебмастер'),
(4, 'admin', 'Администратор'),
(5, 'designer', 'Дизайнер');

-- --------------------------------------------------------

--
-- Структура таблицы `user_wallet`
--

CREATE TABLE IF NOT EXISTS `user_wallet` (
  `uw_id` int(11) NOT NULL AUTO_INCREMENT,
  `uw_title` varchar(200) NOT NULL,
  `uw_wallet` varchar(255) NOT NULL,
  `uw_create_date` int(11) NOT NULL,
  `uw_update_date` int(11) DEFAULT NULL,
  `uw_label` varchar(255) DEFAULT NULL COMMENT 'Поментка для кошелька',
  `uw_user_id` int(11) NOT NULL,
  PRIMARY KEY (`uw_id`),
  KEY `fk_user_wallet_user1_idx` (`uw_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Электронные кошельки юзеров' AUTO_INCREMENT=1 ;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `order`
--
ALTER TABLE `order`
  ADD CONSTRAINT `fk_order_user1` FOREIGN KEY (`o_user_id`) REFERENCES `user` (`u_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_order_shipping1` FOREIGN KEY (`o_shipping`) REFERENCES `shipping` (`s_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_order_order_timeline1` FOREIGN KEY (`o_timeline`) REFERENCES `order_timeline` (`otl_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_order_order_status1` FOREIGN KEY (`o_status`) REFERENCES `order_status` (`os_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_order_order_payment_type1` FOREIGN KEY (`o_payment_type`) REFERENCES `payment_type` (`pt_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_order_order_products1` FOREIGN KEY (`o_id`) REFERENCES `order_products` (`op_order_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `order_products`
--
ALTER TABLE `order_products`
  ADD CONSTRAINT `order_products_ibfk_2` FOREIGN KEY (`op_product_id`) REFERENCES `product` (`p_id`),
  ADD CONSTRAINT `order_products_ibfk_1` FOREIGN KEY (`op_order_id`) REFERENCES `order` (`o_id`);

--
-- Ограничения внешнего ключа таблицы `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `fk_product_product_size1` FOREIGN KEY (`p_size`) REFERENCES `product_size` (`ps_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_product_product_type1` FOREIGN KEY (`p_type`) REFERENCES `product_type` (`pt_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_product_user1` FOREIGN KEY (`p_author_id`) REFERENCES `user` (`u_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_product_product_category1` FOREIGN KEY (`p_category`) REFERENCES `product_category` (`pc_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_product_product_template1` FOREIGN KEY (`p_template`) REFERENCES `product_template` (`ptmpl_int`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `fk_user_user_type` FOREIGN KEY (`u_type`) REFERENCES `user_type` (`ut_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_user_user_lists1` FOREIGN KEY (`u_list`) REFERENCES `user_status_list` (`ul_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `user_wallet`
--
ALTER TABLE `user_wallet`
  ADD CONSTRAINT `fk_user_wallet_user1` FOREIGN KEY (`uw_user_id`) REFERENCES `user` (`u_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
