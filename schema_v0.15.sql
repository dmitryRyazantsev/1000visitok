SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `visitki` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `visitki` ;

-- -----------------------------------------------------
-- Table `visitki`.`user_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `visitki`.`user_type` (
  `ut_id` INT NOT NULL AUTO_INCREMENT,
  `ut_title` VARCHAR(45) NOT NULL,
  `ut_title_ru` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`ut_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `visitki`.`user_status_list`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `visitki`.`user_status_list` (
  `ul_id` INT NOT NULL AUTO_INCREMENT,
  `ul_title` VARCHAR(255) NOT NULL,
  `ul_title_ru` VARCHAR(255) NULL,
  PRIMARY KEY (`ul_id`))
ENGINE = InnoDB
COMMENT = 'Пользовательские списки.\n(черный список, постоянный клиент и /* comment truncated */ /* тп)*/';


-- -----------------------------------------------------
-- Table `visitki`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `visitki`.`user` (
  `u_id` INT NOT NULL AUTO_INCREMENT,
  `u_first_name` VARCHAR(150) NOT NULL,
  `u_second_name` VARCHAR(150) NOT NULL,
  `u_last_name` VARCHAR(150) NOT NULL,
  `u_register_date` INT NOT NULL,
  `u_update_date` INT NULL,
  `u_phone` VARCHAR(15) NOT NULL,
  `u_email` VARCHAR(100) NOT NULL,
  `u_skype` VARCHAR(100) NULL,
  `u_avatar` VARCHAR(255) NULL,
  `u_referer` VARCHAR(255) NULL,
  `u_discount` INT NOT NULL DEFAULT 0 COMMENT 'Скидка',
  `u_type` INT NOT NULL COMMENT 'Тип юзера (менеджер, дизайнер, админ и ...)',
  `u_list` INT NOT NULL DEFAULT 1 COMMENT 'Списко пользователей',
  PRIMARY KEY (`u_id`),
  INDEX `fk_user_user_type_idx` (`u_type` ASC),
  INDEX `fk_user_user_lists1_idx` (`u_list` ASC),
  CONSTRAINT `fk_user_user_type`
    FOREIGN KEY (`u_type`)
    REFERENCES `visitki`.`user_type` (`ut_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_user_lists1`
    FOREIGN KEY (`u_list`)
    REFERENCES `visitki`.`user_status_list` (`ul_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Пользователи';


-- -----------------------------------------------------
-- Table `visitki`.`user_wallet`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `visitki`.`user_wallet` (
  `uw_id` INT NOT NULL AUTO_INCREMENT,
  `uw_title` VARCHAR(200) NOT NULL,
  `uw_wallet` VARCHAR(255) NOT NULL,
  `uw_create_date` INT NOT NULL,
  `uw_update_date` INT NULL,
  `uw_label` VARCHAR(255) NULL COMMENT 'Поментка для кошелька',
  `uw_user_id` INT NOT NULL,
  PRIMARY KEY (`uw_id`),
  INDEX `fk_user_wallet_user1_idx` (`uw_user_id` ASC),
  CONSTRAINT `fk_user_wallet_user1`
    FOREIGN KEY (`uw_user_id`)
    REFERENCES `visitki`.`user` (`u_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Электронные кошельки юзеров';


-- -----------------------------------------------------
-- Table `visitki`.`product_size`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `visitki`.`product_size` (
  `ps_id` INT NOT NULL AUTO_INCREMENT,
  `ps_size` VARCHAR(255) NOT NULL,
  `ps_unit` VARCHAR(45) NOT NULL COMMENT 'еденица измерения',
  PRIMARY KEY (`ps_id`))
ENGINE = InnoDB
COMMENT = 'размеры для товаров';


-- -----------------------------------------------------
-- Table `visitki`.`product_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `visitki`.`product_type` (
  `pt_id` INT NOT NULL AUTO_INCREMENT,
  `pt_name` VARCHAR(255) NOT NULL,
  `pt_name_ru` VARCHAR(255) NULL,
  PRIMARY KEY (`pt_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `visitki`.`product_template`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `visitki`.`product_template` (
  `ptmpl_int` INT NOT NULL AUTO_INCREMENT,
  `ptmpl_img` VARCHAR(255) NOT NULL,
  `ptmp_name` VARCHAR(45) NULL,
  PRIMARY KEY (`ptmpl_int`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `visitki`.`order_products`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `visitki`.`order_products` (
  `op_order_id` INT NOT NULL,
  `op_product_id` INT NOT NULL,
  `op_update_time` INT NOT NULL,
  PRIMARY KEY (`op_order_id`, `op_product_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `visitki`.`product_category`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `visitki`.`product_category` (
  `pc_id` INT NOT NULL AUTO_INCREMENT,
  `pc_name` VARCHAR(255) NOT NULL,
  `pc_name_ru` VARCHAR(255) NULL,
  PRIMARY KEY (`pc_id`))
ENGINE = InnoDB
COMMENT = 'Таблица тегов для товара';


-- -----------------------------------------------------
-- Table `visitki`.`product`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `visitki`.`product` (
  `p_id` INT NOT NULL AUTO_INCREMENT,
  `p_name` VARCHAR(255) NOT NULL,
  `p_price` INT NOT NULL,
  `p_description` TEXT NOT NULL,
  `p_preview_img` VARCHAR(255) NOT NULL,
  `p_published` INT NOT NULL DEFAULT 1 COMMENT 'Опубликован или нет.',
  `p_side` VARCHAR(2) NULL COMMENT 'Колличество сторон/страниц',
  `p_type` INT NOT NULL,
  `p_size` INT NOT NULL COMMENT 'размеры продукта',
  `p_author_id` INT NOT NULL,
  `p_category` INT NOT NULL,
  `p_template` INT NOT NULL,
  PRIMARY KEY (`p_id`),
  INDEX `fk_product_product_size1_idx` (`p_size` ASC),
  INDEX `fk_product_product_type1_idx` (`p_type` ASC),
  INDEX `fk_product_user1_idx` (`p_author_id` ASC),
  INDEX `fk_product_product_category1_idx` (`p_category` ASC),
  INDEX `fk_product_product_template1_idx` (`p_template` ASC),
  CONSTRAINT `fk_product_product_size1`
    FOREIGN KEY (`p_size`)
    REFERENCES `visitki`.`product_size` (`ps_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_product_type1`
    FOREIGN KEY (`p_type`)
    REFERENCES `visitki`.`product_type` (`pt_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_order_products1`
    FOREIGN KEY (`p_id`)
    REFERENCES `visitki`.`order_products` (`op_product_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_user1`
    FOREIGN KEY (`p_author_id`)
    REFERENCES `visitki`.`user` (`u_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_product_category1`
    FOREIGN KEY (`p_category`)
    REFERENCES `visitki`.`product_category` (`pc_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_product_template1`
    FOREIGN KEY (`p_template`)
    REFERENCES `visitki`.`product_template` (`ptmpl_int`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Товары'


-- -----------------------------------------------------
-- Table `visitki`.`shipping`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `visitki`.`shipping` (
  `s_id` INT NOT NULL AUTO_INCREMENT,
  `s_name` VARCHAR(255) NOT NULL,
  `s_price` INT NOT NULL,
  `s_description` TEXT NULL,
  PRIMARY KEY (`s_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `visitki`.`order_timeline`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `visitki`.`order_timeline` (
  `otl_id` INT NOT NULL AUTO_INCREMENT,
  `otl_name` VARCHAR(255) NOT NULL,
  `otl_css_class` VARCHAR(255) NULL,
  PRIMARY KEY (`otl_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `visitki`.`order_status`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `visitki`.`order_status` (
  `os_id` INT NOT NULL AUTO_INCREMENT,
  `os_name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`os_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `visitki`.`payment_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `visitki`.`payment_type` (
  `pt_id` INT NOT NULL AUTO_INCREMENT,
  `pt_name` VARCHAR(255) NOT NULL,
  `pt_description` TEXT NULL,
  `pt_css_class` VARCHAR(255) NULL,
  PRIMARY KEY (`pt_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `visitki`.`order`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `visitki`.`order` (
  `o_id` INT NOT NULL AUTO_INCREMENT,
  `o_user_id` INT NOT NULL,
  `o_shipping` INT NOT NULL,
  `o_status` INT NOT NULL,
  `o_timeline` INT NOT NULL COMMENT 'продолжительность',
  `o_payment_type` INT NOT NULL,
  `o_quantity` INT NOT NULL DEFAULT 1 COMMENT 'Колличество',
  `o_total_price` INT NOT NULL,
  `o_create_date` INT NOT NULL,
  `o_update_date` INT NULL,
  `o_fio` VARCHAR(255) NULL,
  `o_email` VARCHAR(100) NULL,
  `o_phone` VARCHAR(15) NULL,
  `o_shipping_adress` VARCHAR(255) NULL,
  `o_discount` INT NULL,
  `o_document` VARCHAR(255) NULL COMMENT 'Документ со счетом для безнала',
  PRIMARY KEY (`o_id`),
  INDEX `fk_order_user1_idx` (`o_user_id` ASC),
  INDEX `fk_order_shipping1_idx` (`o_shipping` ASC),
  INDEX `fk_order_order_timeline1_idx` (`o_timeline` ASC),
  INDEX `fk_order_order_status1_idx` (`o_status` ASC),
  INDEX `fk_order_order_payment_type1_idx` (`o_payment_type` ASC),
  CONSTRAINT `fk_order_user1`
    FOREIGN KEY (`o_user_id`)
    REFERENCES `visitki`.`user` (`u_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_order_shipping1`
    FOREIGN KEY (`o_shipping`)
    REFERENCES `visitki`.`shipping` (`s_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_order_order_timeline1`
    FOREIGN KEY (`o_timeline`)
    REFERENCES `visitki`.`order_timeline` (`otl_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_order_order_status1`
    FOREIGN KEY (`o_status`)
    REFERENCES `visitki`.`order_status` (`os_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_order_order_payment_type1`
    FOREIGN KEY (`o_payment_type`)
    REFERENCES `visitki`.`payment_type` (`pt_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_order_order_products1`
    FOREIGN KEY (`o_id`)
    REFERENCES `visitki`.`order_products` (`op_order_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Data for table `visitki`.`user_type`
-- -----------------------------------------------------
START TRANSACTION;
USE `visitki`;
INSERT INTO `visitki`.`user_type` (`ut_id`, `ut_title`, `ut_title_ru`) VALUES (NULL, 'client', 'Клиент');
INSERT INTO `visitki`.`user_type` (`ut_id`, `ut_title`, `ut_title_ru`) VALUES (NULL, 'manager', 'Менеджер');
INSERT INTO `visitki`.`user_type` (`ut_id`, `ut_title`, `ut_title_ru`) VALUES (NULL, 'webmaster', 'Вебмастер');
INSERT INTO `visitki`.`user_type` (`ut_id`, `ut_title`, `ut_title_ru`) VALUES (NULL, 'admin', 'Администратор');
INSERT INTO `visitki`.`user_type` (`ut_id`, `ut_title`, `ut_title_ru`) VALUES (NULL, 'designer', 'Дизайнер');

COMMIT;


-- -----------------------------------------------------
-- Data for table `visitki`.`user_status_list`
-- -----------------------------------------------------
START TRANSACTION;
USE `visitki`;
INSERT INTO `visitki`.`user_status_list` (`ul_id`, `ul_title`, `ul_title_ru`) VALUES (NULL, 'usual', 'Обычные');
INSERT INTO `visitki`.`user_status_list` (`ul_id`, `ul_title`, `ul_title_ru`) VALUES (NULL, 'blacklist', 'Blacklist');
INSERT INTO `visitki`.`user_status_list` (`ul_id`, `ul_title`, `ul_title_ru`) VALUES (NULL, 'locked', 'Заблокирован');

COMMIT;


-- -----------------------------------------------------
-- Data for table `visitki`.`product_size`
-- -----------------------------------------------------
START TRANSACTION;
USE `visitki`;
INSERT INTO `visitki`.`product_size` (`ps_id`, `ps_size`, `ps_unit`) VALUES (NULL, '25x60', 'мм');
INSERT INTO `visitki`.`product_size` (`ps_id`, `ps_size`, `ps_unit`) VALUES (NULL, '30x10', 'см');

COMMIT;


-- -----------------------------------------------------
-- Data for table `visitki`.`product_type`
-- -----------------------------------------------------
START TRANSACTION;
USE `visitki`;
INSERT INTO `visitki`.`product_type` (`pt_id`, `pt_name`, `pt_name_ru`) VALUES (NULL, 'Визитки', NULL);
INSERT INTO `visitki`.`product_type` (`pt_id`, `pt_name`, `pt_name_ru`) VALUES (NULL, 'Календари', NULL);
INSERT INTO `visitki`.`product_type` (`pt_id`, `pt_name`, `pt_name_ru`) VALUES (NULL, 'Флаеры', NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `visitki`.`product_category`
-- -----------------------------------------------------
START TRANSACTION;
USE `visitki`;
INSERT INTO `visitki`.`product_category` (`pc_id`, `pc_name`, `pc_name_ru`) VALUES (NULL, 'Природа', NULL);
INSERT INTO `visitki`.`product_category` (`pc_id`, `pc_name`, `pc_name_ru`) VALUES (NULL, 'Перевозки', NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `visitki`.`shipping`
-- -----------------------------------------------------
START TRANSACTION;
USE `visitki`;
INSERT INTO `visitki`.`shipping` (`s_id`, `s_name`, `s_price`, `s_description`) VALUES (NULL, 'Самовывоз', 0, NULL);
INSERT INTO `visitki`.`shipping` (`s_id`, `s_name`, `s_price`, `s_description`) VALUES (NULL, 'Доставка курьером', 300, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `visitki`.`order_timeline`
-- -----------------------------------------------------
START TRANSACTION;
USE `visitki`;
INSERT INTO `visitki`.`order_timeline` (`otl_id`, `otl_name`, `otl_css_class`) VALUES (NULL, '4 часа', NULL);
INSERT INTO `visitki`.`order_timeline` (`otl_id`, `otl_name`, `otl_css_class`) VALUES (NULL, '2 дня', NULL);
INSERT INTO `visitki`.`order_timeline` (`otl_id`, `otl_name`, `otl_css_class`) VALUES (NULL, '4 дня', NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `visitki`.`order_status`
-- -----------------------------------------------------
START TRANSACTION;
USE `visitki`;
INSERT INTO `visitki`.`order_status` (`os_id`, `os_name`) VALUES (NULL, 'в обработке');
INSERT INTO `visitki`.`order_status` (`os_id`, `os_name`) VALUES (NULL, 'передан курьеру');
INSERT INTO `visitki`.`order_status` (`os_id`, `os_name`) VALUES (NULL, 'ожидает подтверждения');

COMMIT;


-- -----------------------------------------------------
-- Data for table `visitki`.`payment_type`
-- -----------------------------------------------------
START TRANSACTION;
USE `visitki`;
INSERT INTO `visitki`.`payment_type` (`pt_id`, `pt_name`, `pt_description`, `pt_css_class`) VALUES (NULL, 'Курьеру', NULL, NULL);
INSERT INTO `visitki`.`payment_type` (`pt_id`, `pt_name`, `pt_description`, `pt_css_class`) VALUES (NULL, 'Webmoney', NULL, NULL);
INSERT INTO `visitki`.`payment_type` (`pt_id`, `pt_name`, `pt_description`, `pt_css_class`) VALUES (NULL, 'Б/н расчет', NULL, NULL);

COMMIT;

